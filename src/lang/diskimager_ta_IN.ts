<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ta_IN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="29"/>
        <source>Win32 Disk Imager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="54"/>
        <source>Image File</source>
        <translation>படத்தின் கோப்பு</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="78"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="92"/>
        <source>Device</source>
        <translation>சாதனம்</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="128"/>
        <source>Hash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="148"/>
        <source>Select hash type for verification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="152"/>
        <source>None</source>
        <translation>ஏதுமில்லை</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="166"/>
        <source>Generate selected hash on file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="172"/>
        <source>Generate</source>
        <translation>உருவாக்கு</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="185"/>
        <source>Copy hash to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="191"/>
        <source>Copy</source>
        <translation>பிரதி</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="246"/>
        <source>Read Only Allocated Partitions</source>
        <translation>ஒதுக்கப்பட்ட பகிர்வுகளை மட்டும் வாசி</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="268"/>
        <source>Progress</source>
        <translation>முன்னேற்றம்</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="295"/>
        <source>%p%</source>
        <translation>%p%</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <source>Cancel current process.</source>
        <translation>தற்போதைய செயல்முறை ரத்துக்கு.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="332"/>
        <source>Cancel</source>
        <translation>ரத்து</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="348"/>
        <source>Read data from &apos;Device&apos; to &apos;Image File&apos;</source>
        <translation>&apos;சாதனம்&apos;-ல் இருந்து &apos;பட கோப்பு&apos;-க்கு தரவுகளை மாற்று</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="351"/>
        <source>Read</source>
        <translation>படி</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="364"/>
        <source>Write data from &apos;Image File&apos; to &apos;Device&apos;</source>
        <translation>&apos;பட கோப்பு&apos;-ல் இருந்து &apos;சாதனம்&apos;-க்கு தரவுகளை மாற்று</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="367"/>
        <source>Write</source>
        <translation>எழுது</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="380"/>
        <source>Exit Win32 Disk Imager</source>
        <translation type="unfinished">வெளியேறு</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="383"/>
        <source>Exit</source>
        <translation>வெளியேறு</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="52"/>
        <source>Waiting for a task.</source>
        <translation>ஒரு பணி காத்திருக்கிறது.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="131"/>
        <source>Downloads$</source>
        <translation>பதிவிறக்கம்$</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="167"/>
        <location filename="mainwindow.cpp" line="177"/>
        <source>Exit?</source>
        <translation>வெளியேறு?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="167"/>
        <source>Exiting now will result in a corrupt image file.
Are you sure you want to exit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="177"/>
        <source>Exiting now will result in a corrupt disk.
Are you sure you want to exit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="195"/>
        <source>Disk Images (*.img *.IMG);;*.*</source>
        <translation>வட்டு படங்கள் (*.img *.IMG);;*.*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="197"/>
        <source>Select a disk image</source>
        <translation>ஒரு வட்டு படத்தை தேர்ந்தெடு</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="240"/>
        <source>Generating...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="274"/>
        <source>Cancel?</source>
        <translation>ரத்து?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="274"/>
        <source>Canceling now will result in a corrupt destination.
Are you sure you want to cancel?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="301"/>
        <location filename="mainwindow.cpp" line="550"/>
        <location filename="mainwindow.cpp" line="650"/>
        <source>Write Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="301"/>
        <location filename="mainwindow.cpp" line="550"/>
        <source>Image file cannot be located on the target device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="311"/>
        <source>Confirm overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="311"/>
        <source>Writing to a physical device can corrupt the device.
(Target Device: %1 &quot;%2&quot;)
Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="417"/>
        <source>Not enough available space!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="503"/>
        <location filename="mainwindow.cpp" line="508"/>
        <location filename="mainwindow.cpp" line="513"/>
        <location filename="mainwindow.cpp" line="527"/>
        <source>File Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="503"/>
        <source>The selected file does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="508"/>
        <source>You do not have permision to read the selected file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="513"/>
        <source>The specified file contains no data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="517"/>
        <location filename="mainwindow.cpp" line="729"/>
        <source>Done.</source>
        <translation>முடிந்தது.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="521"/>
        <location filename="mainwindow.cpp" line="733"/>
        <location filename="mainwindow.cpp" line="735"/>
        <source>Complete</source>
        <translation>முடி</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="521"/>
        <source>Write Successful.</source>
        <translation>வெற்றிகரமாக எழுது.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="527"/>
        <source>Please specify an image file to use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="556"/>
        <source>Confirm Overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="556"/>
        <source>Are you sure you want to overwrite the specified file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="650"/>
        <source>Disk is not large enough for the specified image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="733"/>
        <source>Read Canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="735"/>
        <source>Read Successful.</source>
        <translation>வெற்றிகரமாக படி.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="742"/>
        <source>File Info</source>
        <translation>கோப்பன் தகவல்</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="742"/>
        <source>Please specify a file to save data to.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="disk.cpp" line="42"/>
        <location filename="disk.cpp" line="229"/>
        <location filename="disk.cpp" line="318"/>
        <location filename="disk.cpp" line="333"/>
        <source>File Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="42"/>
        <source>An error occurred when attempting to get a handle on the file.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="59"/>
        <location filename="disk.cpp" line="206"/>
        <source>Device Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="60"/>
        <source>An error occurred when attempting to get a handle on the device.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="78"/>
        <location filename="disk.cpp" line="410"/>
        <source>Volume Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="79"/>
        <source>An error occurred when attempting to get a handle on the volume.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="96"/>
        <source>Lock Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="97"/>
        <source>An error occurred when attempting to lock the volume.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="114"/>
        <source>Unlock Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="115"/>
        <source>An error occurred when attempting to unlock the volume.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="132"/>
        <source>Dismount Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="133"/>
        <source>An error occurred when attempting to dismount the volume.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="160"/>
        <source>Read Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="161"/>
        <source>An error occurred when attempting to read data from handle.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="187"/>
        <source>Write Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="188"/>
        <source>An error occurred when attempting to write data to handle.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="207"/>
        <source>An error occurred when attempting to get the device&apos;s geometry.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="230"/>
        <source>An error occurred while getting the file size.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="252"/>
        <source>Free Space Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="253"/>
        <source>Failed to get the free space on drive %1.
Error %2: %3
Checking of free space will be skipped.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="319"/>
        <source>An error occurred while getting the device number.
This usually means something is currently accessing the device;please close all applications and try again.

Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="334"/>
        <source>An error occurred while querying the properties.
This usually means something is currently accessing the device; please close all applications and try again.

Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="disk.cpp" line="411"/>
        <source>An error occurred when attempting to get a handle on %3.
Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
